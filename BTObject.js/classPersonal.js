// Write a Pesonal class have:

// Variable: ID, fullName, email, birthday{day,month,year}, position, seniority

// Function:
//     1. contructor(): Set all value of variable.
//         Note: Parameter must have default value.
//         Then declare 3 instance by this class.
//     2. get age():
//     3. get salary():
//         Calculate Salary just like we did personal object.
//         Salary by position: {
//                 manager: 1000 USD,
//                 secterary: 400 USD,
//                 employee: 300 USD
//             }
//         Salary by seniority: { 
//                 Under 12 month: Salary coefficient = 1.2
//                 12 month -> 24 month: Salary coefficient = 2.0
//                 24 month -> 36 month: Salary coefficient = 3.4
//                 Over 36 month: Salary coefficient = 4.5
//             }
//     4. static highestSalary(): Parameter is a array have all instance person.
//         Then find person have highest salary

// Final: Call all function one by one at outside of the class.



class Personal {

    constructor(ID, fullName, email, birthday, position, seniority) {
        this.ID = ID;
        this.fullName = fullName;
        this.email = email;
        this.birthday = birthday;
        this.position = position;
        this.seniority = seniority;
    }
    get age() {
        let today = new Date();
        let bday = new Date(this.birthday) // array = ['25','6','1997']
        return today.getFullYear() - bday.getFullYear()
    }
    get salary() {
       let positions = {
            manager: 1000,
            secterary: 400,
            employee: 300
        }
        let sal = positions[this.position];
        if (this.seniority < 12) {
            return sal * 1.2;
        } else if (this.seniority <= 24) {
            return sal * 2;
        } else if (this.seniorityy <= 36) {
            return sal * 3.4;
        } else {
            return sal * 4.5;
        }
    }
    static highestSalary(arr) { //
        let max=0;
        let dem=0;
       arr.forEach((element,index )=> {
           if(max<element.salary){
               max=element.salary;
               index=dem;
           }
       });
       return arr[dem];
    }
}
let personal1 = new Personal("121", "trang", "huyentrang@gmail.com", "06/25/1997", "manager", "42");//khai bao 1 instance
let personal2 = new Personal("124", "hanh", "hanh@gmail.com", "12/12/1998", "secterary", "36")
let personal3 = new Personal("144", "huyen", "huyenhuyen@gmail.com", "02/10/1996", "employee", "15")
let arr = [personal1, personal2, personal3];
console.log(personal1.age);
console.log(personal1.salary);
console.log(Personal.highestSalary(arr));




