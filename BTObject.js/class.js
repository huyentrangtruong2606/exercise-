//class
class Rectangle{
    constructor(height,width){  //1 class chi co 1 construction(noi bo phai thong qua),1 loai la static(cach goi:class.)
        this.height=height;     //construction la 1 function de khoi tao 1 class.function dung de goi
        this.width=width;
    }
    static square(height,width){   //static duoc goi truc tiep . moi static la co nhieu instance 
           
    }
    get area(){                     //bien.return ve 1 bien
        return this.calcAre();//
    }
    calcAre(){                      // method :gan gia tri,function
        return this.height*this.width;
    }
}
const square=new Rectangle(10,10);
console.log(square.calcAre());

console.log(Rectangle.name);           //lay ten class =>Rectangle



//class1:
class Student{
    constructor(firstname,lastname,birthday){
        this.firstname=firstname;
        this.lastname=lastname;
        this.birthday=birthday;
    }
    displayFullname(){                          //function
        console.log(`${this.firstname} ${this.lastname}`);
        
    }
    get age(){                                  //function return dang bien 
        let today=new Date();                       //set gan gia tri cho du lieu
        return today.getFullYear()-this.birthday;
    }
    static getName(student){
        return student.lastname;
    }
    toString(){
        return this.firstname+''+this.lastname+''+this.birthday;
    }
    // static compareAge(student1,student2){
    //     if(student1.age>student2.age){
    //         console.log("student1 gia hon");   
    //     }else console.log("student2 gia hon");  
    // }
}
let student1=new Student("trang","B","1998");//khai bao 1 instance 
let student2=new Student("nguyen","A","1997")
console.log(student1.firstname);
console.log(student1.age);
student1.displayFullname();
console.log(Student.getName(student1));
//Student.compareAge(student1,student2);


//prototype:lop cha
//attack():ke thua (455)
//extends():ke thua
//hasOwnProperty: ktra su ton tai .no co chua bien 
//toStrinng:chuyen mang hoac so hoac all ve 1 string
//trim:bo cach .vd:"  hello  ".trim =>"hello"
arr.prototype.delete=function(idx){ //472
    return this.splice(0,idx);
}

let arr=[1,4,5,7];
