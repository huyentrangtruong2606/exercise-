// Write Student class have:

// Variable: ID, firstName, lastName, scores{math,physical,chemistry}

// Function:
//     1. constructor()
//     2. get averageScore()
//     3. get studentTitle(): return title of student, need follow this formula
//         if averageScore < 5: Bad Student
//         if 5 <= averageScore < 6.5 and don't have any subject score under 3.5: Average Student
//         if 6.5 <= averageScore < 8 and don't have any subject score under 5: Good Student
//         if averageScore >= 8 and don't have any subject score under 6.5: Excellent student
//     4. static bestStudents(): Parameter is all of students.
//         Then return 10% students have best average score.

// Final: Declare instance by this class and call all function.
class Student{
    constructor(ID, firstName, lastName, scores){
        this.ID=ID;
        this.firstName=firstName;
        this.lastName=lastName;
        this.scores=scores;
    }
    get averageScore(){
        let sum=0;
        let count=0;
        Object.values(this.scores).forEach(element => {
            sum+=element;
            count++;
        })
        return sum/count;
    }
    check(a){
        Object.values(this.scores).forEach(element => {
            if(element<a)  return true;
              
        })
        return false;
    }
    get studentTitle(){
        switch(true){
            case(this.averageScore<5):{
                return "Bad Student"; 
            } 
            case(this.averageScore<6.5):{
                if(this.check(3.5)) return "Bad Student";
                return "Average Student";
            }
            case(this.averageScore<8):{
                switch(true){
                    case(this.check(3.5)): return "Bad Student";
                    case(this.check(5)): return "Average Student";
                    default: return "Good Student ";

                }
            }
            default:{
                switch(true){
                    case(this.check(3.5)): return "Bad Student";
                    case(this.check(5)): return "Average Student";
                    case(this.check(6.5)): return "Good Student";
                    default: return "Excellent";
                }
            }
            }
        }
        static bestStudents(arr){
           //sort list of student by average score property
            arr.sort((a,b)=>(a.averageScore < b.averageScore ? 1 : b.averageScore < a.averageScore ? -1 : 0))
            return arr.slice(0,Math.ceil(arr.length *0.1))
        }
    }
let student1=new Student('123','huyen','trang',{math:5,physical:7,chemistry:8});
let student2=new Student('123','huyen','trang',{math:4,physical:8,chemistry:9});
let student3=new Student('123','huyen','trang',{math:6,physical:6,chemistry:4});
let arr=[student1,student2,student3];
console.log(student1.averageScore);
console.log(student1.studentTitle);
console.log(Student.bestStudents(arr));


