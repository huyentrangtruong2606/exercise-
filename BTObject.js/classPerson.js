// 1.Create Person Class have:
//     Variable: firstName, lastName, age, gender, interests
//     Function:
//      + constructor()
// 2. Create Teacher Class extends Person Class:
//     Variable: subject,grade
//     Function:
//      + constructor(): use super() function to call Person constructor
//      + get subject(): return subject
// 3. Create Student Class extends Person Class:
//     Variable: scores, title
//     Function:
//      + constructor(): use super() function
//      + get title(): return title
    
// Final: Declare 2 instance by 2 class Teacher & Student. Then call function.

class Person{
    constructor(firstName,lastName,age,gender,interests){
        this.firstName=firstName;
        this.lastName=lastName;
        this.age=age;
        this.gender=gender;
        this.interests=interests;
    }

}
class Teacher extends Person{
    constructor(firstName,lastName,age,gender,interests,subject,grade){
        super(firstName,lastName,age,gender,interests);
        this.subject =subject;
        this.grade=grade;
    }
    get Subject(){
        return this.subject;
    }
}
class Student extends Person{
    constructor( scores, title){
        super(firstName,lastName,age,gender,interests);
        this.scores=scores;
        this.title=title;
    }
    get Title(){
        return this.title;
    }

}
let teacher1=new Teacher("huyen","trang","18","1","doc sach","xx","ss");
console.log(teacher1.Subject);
