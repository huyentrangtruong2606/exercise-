let personnel = [
    {
        ID: 3123,
        fullName: "Phan Van Teo",
        email: "teo_em@gmail.com",
        birthday: {
            day: 12,
            month: 3,
            year: 1980
        },
        age: 0,
        gender: "male",
        nativeLand: "Quang Nam Provice",
        position: "manager",
        salary: 0,
        seniority: 31
    },
    {
        ID: 3112,
        fullName: "Ban Thi No",
        email: "thi_no@gmail.com",
        birthday: {
            day: 4,
            month: 4,
            year: 1987
        },
        age: 0,
        gender: "female",
        nativeLand: "Ha Tinh Provice",
        position: "secterary",
        salary: 0,
        seniority: 20
    },
    {
        ID: 3376,
        fullName: "Ton That Hoc",
        email: "that.hoc@gmail.com",
        birthday: {
            day: 12,
            month: 3,
            year: 1998
        },
        age: 0,
        gender: "male",
        nativeLand: "Da Nang City",
        position: "employee",
        salary: 0,
        seniority: 9
    },
    {
        ID: 3377,
        fullName: "Ton That Nghiep",
        email: "that.nghiep@gmail.com",
        birthday: {
            day: 29,
            month: 2,
            year: 1996
        },
        age: 0,
        gender: "female",
        nativeLand: "Da Nang City",
        position: "employee",
        salary: 0,
        seniority: 50
    }
    ]
    // Salary by position: {
    //     manager: 1000 USD,
    //     secterary: 400 USD,
    //     employee: 300 USD
    // }
    // Salary by seniority: {
    //     Under 12 month: Salary coefficient = 1.2
    //     12 month -> 24 month: Salary coefficient = 2.0
    //     24 month -> 36 month: Salary coefficient = 3.4
    //     Over 36 month: Salary coefficient = 4.5
    // }

let position={
    manage:1000,
    secterary: 400,
    employee: 300
}
function calculateSalary(person)
{
    let arr =Object.keys(position);
    for(let key of arr){
        if(person.position==key)
        {
            person.salary=position[key]
            //position[key]: lay value cua key trog position
        }
    }
    //switch(true){
    // case(person.seniority<12):
    // 
    //     salary*=1.2;
    //      break;
    // case(person.seniority<=24):
    // 
    //     salary*=2;
    //      break;
    // case(person.seniority<=36):
    // 
    //     salary*=3.4;
    //      break;
    //default:
    //     salary*=4.5;
    // }

        if(person.seniority<12)
        {
            salary*=1.2;
        }else if(person.seniority<=24)
        {
            salary*=2;
        }else if(person.seniority<=36)
        {
            salary*=3.4;
        }else{
            salary*=4.5;
        }
    }


for(let person of personnel)
{
    person.salary=calculateSalary(person)
}