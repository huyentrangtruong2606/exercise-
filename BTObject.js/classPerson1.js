// 1. Create Person Class have:
//     Variable: firstName, lastName, birthday, gender
//     Function:
//      + constructor()
//      + getAge()

// 2. Create Employer extends Person have:
//     Variable: position, company, workHours *->permonth
//     Function: 
//      + constructor()
//      + get salary() by position: {
//                 manager: 10 USD/hour,
//                 secterary: 7 USD/hour,
//                 employee: 4 USD/hour
//             } and multiply with workHours.

// 3. Create Investors extends Person have:
//     Variable: numberOfStocks
//     Function:
//     + constructor()
//     + get percent of stocks:
//         declare all stocks of company, then divide it.

// P/s: Call all function to test code.

class Person{
    constructor(firstName, lastName, birthday, gender){
        this.firstName=firstName;
        this.lastName=lastName;
        this.birthday=birthday;
        this.gender=gender;
    }
    get Age(){
        let today=new Date();
        let bday = new Date(this.birthday) // array = ['25','6','1997']
        return today.getFullYear() - bday.getFullYear()
    }
}
class Employer extends Person{
    constructor( firstName, lastName, birthday, gender,position, company, workHours){
        super(firstName, lastName, birthday, gender);
        this.position=position;
        this.company=company;
        this.workHours=workHours;
    }
    get salary(){
        let positions = {
            manager: 10,
            secterary: 4,
            employee: 3
        }
        let p = positions[this.position];
            return p*this.workHours;
        
    }
}
class Investors extends Person{
    constructor(firstName, lastName, birthday, gender,numberOfStocks){
        super(firstName, lastName, birthday, gender);
        this.numberOfStocks=numberOfStocks;
    }
    get percent(){
        let stocks =1000;
        return this.numberOfStocks/stocks;
    }
}
let person1=new Person('huyen','trang','1997','nu');
let employer1=new Employer('huyen','trang','1997','nu','manager','ttt','7');
let investors=new Investors('huyen','trang','1997','nu','2000');
console.log(person1.Age);
console.log(employer1.salary);
console.log(investors.percent);


